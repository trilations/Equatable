<?php

namespace Trilations\Equatable\Tests\Traits;

use PHPUnit\Framework\TestCase;
use Trilations\Equatable\Traits\EquatableTrait;

class EquatableTraitTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testEqualsSameIdTrue()
    {
        $traitMock1 = $this->getMockForTrait(EquatableTrait::class);
        $traitMock2 = clone $traitMock1;
        $traitMock1->expects($this->any())->method('getId')->willReturn(1);
        $traitMock2->expects($this->any())->method('getId')->willReturn(1);

        $this->assertTrue($traitMock1->equals($traitMock2));
    }

    /**
     * @throws \ReflectionException
     */
    public function testEqualsSameIdFalse()
    {
        $traitMock1 = $this->getMockForTrait(EquatableTrait::class);
        $traitMock2 = clone $traitMock1;
        $traitMock1->expects($this->any())->method('getId')->willReturn(1);
        $traitMock2->expects($this->any())->method('getId')->willReturn(2);

        $this->assertFalse($traitMock1->equals($traitMock2));
    }

    /**
     * @throws \ReflectionException
     */
    public function testEqualsSameClassFalse()
    {
        $traitMock = $this->getMockForTrait(EquatableTrait::class);
        $traitMock->expects($this->any())->method('getId')->willReturn(1);

        $objectMock = $this->getMockBuilder('SomeClass')->setMethods(array('getId'))->getMock();
        $objectMock->expects($this->any())->method('getId')->willReturn(1);

        $this->assertFalse($traitMock->equals($objectMock));
    }


    /**
     * @throws \ReflectionException
     */
    public function testEqualsSameHashTrue()
    {
        $traitMock1 = $this->getMockForTrait(EquatableTrait::class);
        $traitMock1->expects($this->any())->method('getId')->willReturn(1);
        $traitMock2 = $traitMock1;

        $this->assertTrue($traitMock1->equals($traitMock2));
    }
}
