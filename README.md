# Color

A library (composer package) for managing the EquatableInterface and EquatableTrait.

Equatable can indicate whether the current object is equal to another object.

## Installation

Add it to your `composer.json`:
```json
{
    "repositories": [
        {
        "type": "vcs",
        "url":  "https://gitlab.com/trilations/equatable.git"
        }
    ],
    "require": {
        "trilations/equatable": "^2.0"
    }
}
```

And run composer update:
```sh
composer update
```

## Usage

```php
use Trilations\Equatable\Interfaces\EquatableInterface;
use Trilations\Equatable\Traits\EquatableTrait;

// Say, we have 2 objects ($a and $b) that we want to compare
// And $a implements EquatableInterface
if ($a instanceof EquatableInterface) {
    $result = $a->equals($b);
    return $result;
}

```

## Contributing

### Installation

```sh
# make sure you have php 7.4 or higher
php -v

# make sure you have composer installed
composer -v

# clone the repository
git clone git@gitlab.com:trilations/Equatable.git

# cd into the directory
cd Equatable

# install dependencies
composer install
```

### Tests

```sh
# running all tests
./vendor/bin/phpunit

# checking the code coverage (requires Xdebug)
./vendor/bin/phpunit --coverage-text
./vendor/bin/phpunit --coverage-html coverage.html # generate html report

# you might need to prefix these commands with XDEBUG_MODE=coverage
XDEBUG_MODE=coverage ./vendor/bin/phpunit --coverage-text
```

Additionally, you should regularly check for any incompatibilies with our supported PHP versions:
```sh
# Runs the sniffer script as defined in composer.json
composer sniffer:php7.4
composer sniffer:php8.0
composer sniffer:php8.1
```
