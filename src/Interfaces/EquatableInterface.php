<?php

namespace Trilations\Equatable\Interfaces;

interface EquatableInterface
{
    /**
     * Indicates whether the current object is equal to another object.
     * @param $object
     * @return bool
     */
    public function equals($object);
}
