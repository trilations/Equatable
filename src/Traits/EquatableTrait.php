<?php

namespace Trilations\Equatable\Traits;

/**
 * Trait EquatableTrait
 * @package Trilations\Equatable\Traits
 */
trait EquatableTrait
{
    abstract public function getId();

    /**
     * @param $object
     * @return bool
     */
    public function equals($object)
    {
        // if same object
        if (spl_object_hash($object) === spl_object_hash($this)) {
            return true;
        }

        // not the same class: FALSE
        if (get_class($this) !== get_class($object)) {
            return false;
        }

        // same id: TRUE
        if (method_exists($object, 'getId') && $object->getId() == $this->getId()) {
            return true;
        }

        // default: FALSE
        return false;
    }
}
